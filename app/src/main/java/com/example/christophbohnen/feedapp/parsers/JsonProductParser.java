package com.example.christophbohnen.feedapp.parsers;

import android.util.Log;

import com.example.christophbohnen.feedapp.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class JsonProductParser {


    public static List<Product> parseFeed(String content){
        JSONArray ar = null;
        try {
            ar = new JSONArray(content);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("JSONException","maybe no good json --- JSON : " + content);

        }
        List<Product> productsList = new ArrayList<>();

        for (int i = 0; i < ar.length();i++){
            JSONObject obj = null;
            try {
                obj = ar.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Product product = new Product();

            try {
                product.setProductName(obj.getString("name"));
                product.setProductPrice(obj.getDouble("price"));
                product.setProductImageLink(obj.getString("img"));
                productsList.add(product);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        return productsList;
    }
}

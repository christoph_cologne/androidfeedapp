package com.example.christophbohnen.feedapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.example.christophbohnen.feedapp.model.Product;
import com.example.christophbohnen.feedapp.network.HttpManager;
import com.example.christophbohnen.feedapp.parsers.JsonProductParser;
import java.util.ArrayList;
import java.util.List;


public class ListActivity extends android.app.ListActivity {

    ProgressBar pb;
    List<JSONAsyncTask> tasks;
    List<Product> productList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        pb = (ProgressBar) findViewById(R.id.progressBar1);
        pb.setVisibility(View.INVISIBLE);

        tasks = new ArrayList<>();


        if (isOnline()) {
            Log.d("button", "request pressed");
            requestData("http://192.168.178.23/symfony2/web/mongoquery/");
        } else {
            Toast.makeText(this, "Network isn't available", Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_get_data:
                if (isOnline()) {
                    Log.d("button", "request pressed");
                    requestData("http://192.168.178.23/symfony2/web/mongoquery/");
                } else {
                    Toast.makeText(this, "Network isn't available", Toast.LENGTH_LONG).show();
                }
            break;
            case R.id.action_grid:
//                GridView gridView;
//                gridView = (GridView) this.findViewById(R.id.gridView1);
                Log.d("button", "gridlist pressed");
            break;
        }
        return false;
    }


    private boolean isOnline(){

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnectedOrConnecting()){
            return true;
        }
        else {
            return false;
        }
    }

    private void requestData(String uri){

       new JSONAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,uri);

    }

    class JSONAsyncTask extends AsyncTask<String, String, List<Product>> {

//        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (tasks.size() == 0){
                pb.setVisibility(View.VISIBLE);
            }

            tasks.add(this);
        }

        @Override
        protected List<Product> doInBackground(String... urls) {
//          String content = HttpManager.getDataWithAndroidHttpClient(urls[0]);
            String content = HttpManager.getDataWithHttpURLConnection(urls[0]);

            Log.d("Content", content);
            productList = JsonProductParser.parseFeed(content);
            return productList;
        }

        @Override
        protected void onPostExecute(List<Product> products) {
            tasks.remove(this);

            if(tasks.size() == 0){
                pb.setVisibility(View.INVISIBLE);
            }

            updateDisplay();
//            if(result == "")
//                Toast.makeText(getApplicationContext(), "Unable to fetch data from server", Toast.LENGTH_LONG).show();

        }
    }


    protected void updateDisplay(){

        ProductListAdapter productAdapter = new ProductListAdapter(this,R.layout.row_layout,productList);
        setListAdapter(productAdapter);
    }

}

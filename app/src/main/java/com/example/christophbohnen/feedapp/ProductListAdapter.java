package com.example.christophbohnen.feedapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.christophbohnen.feedapp.model.Product;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class ProductListAdapter extends ArrayAdapter<Product>{

    private Context context;
    private List<Product> productsList;
    public static final String BASE_URL = "http://192.168.178.23/symfony2/web/images/";

    public ProductListAdapter(Context context, int resource, List<Product> objects) {
        super(context, resource, objects);
        this.context = context;
        this.productsList = objects;
    }


    @Override
    public View getView(int position,View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.row_layout,parent,false);


        Product product = productsList.get(position);
        TextView nameView = (TextView) view.findViewById(R.id.nameView);
        TextView priceView = (TextView) view.findViewById(R.id.priceView);

        nameView.setText(product.getProductName());
        priceView.setText(Double.toString(product.getProductPrice()) + " €");


        if (product.getBitmap() != null){
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView1);
            imageView.setImageBitmap(product.getBitmap());
        }
        else {
            ProductAndView container = new ProductAndView();
            container.product = product;
            container.view = view;

            ImageLoader loader = new ImageLoader();
            loader.execute(container);
        }

        return view;
    }

    class ProductAndView {
        public Product product;
        public View view;
        public Bitmap bitmap;
    }

    private class ImageLoader extends AsyncTask<ProductAndView,Void,ProductAndView>{

        @Override
        protected ProductAndView doInBackground(ProductAndView... params) {

            ProductAndView container = params[0];
            Product product = container.product;


                try {

                    String imgURL = BASE_URL + product.getProductImageLink()+ ".jpg";
                    InputStream stream = (InputStream) new URL(imgURL).getContent();

                        Bitmap bitmap = BitmapFactory.decodeStream(stream);
                        product.setBitmap(bitmap);
                        container.bitmap = bitmap;

                    stream.close();
                    return container;
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

            return null;
        }

        @Override
        protected void onPostExecute(ProductAndView result) {
            super.onPostExecute(result);

            if(result != null){
                ImageView imageView = (ImageView) result.view.findViewById(R.id.imageView1);
                imageView.setImageBitmap(result.bitmap);
                result.product.setBitmap(result.bitmap);
            }

        }
    }

}

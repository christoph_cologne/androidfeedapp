# Android application #

Implements Retrofit to download and parse JSON and displays the content in a GridView within a SwipeRefreshLayout

### GridActivity ###

* SwipeRefreshLayout to enable pull to refresh
* Profressbar to show download progress
* GridView can be changed to ListView
* Layout is set in XML

### DetailActivity ###

* Encompasses vertical ScrollView
* Viewpager to display horizontal pagination of ImageViews
* Layout is set in XML